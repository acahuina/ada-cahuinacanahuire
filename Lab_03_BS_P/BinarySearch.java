import java.util.*;

public class BinarySearch{
  
  public static int[] Search(int[]array,int element){
    int left=0;
    int right=array.length-1;
    int p=element-1;
    int centinela=1;

     while(array[p]!=element || left<right){
       centinela=centinela+1;
       p=(left+right)/2;
       if(element>array[p])
         left=p+1;
       else
         right=p-1;
     }
    

    if(array[p]!=element)
       p=0;
    else
      p=1;

    int[]rp={p,centinela};
    
    return rp;
  }

}
