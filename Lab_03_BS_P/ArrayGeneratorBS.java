import java.util.*;
import java.io.*;

public class ArrayGeneratorBS{
  private static int[]generateArray(int size){
    int[]array=new int[size];
    for(int i=0;i<size;i++)
      array[i]=i+1;
    return array;
  }

  public static void print(PrintStream out,int[]a){
    int i;
    for(i=0;i<a.length-1;i++){
      out.print(a[i]+" ");
    }
    out.println(a[i]);
  }
  public static void main(String[]args){
    for(int i=1;i<=100;i++){
      System.out.println(i);
      print(System.out,generateArray(i));
    }
  }
}
