import java.util.*;

public class Searching{
  public static void main(String[]args){
    Scanner sc=new Scanner(System.in);
    BinarySearch bs=new BinarySearch(); 
    while(sc.hasNext()){
      int size=sc.nextInt();
      int[]a=new int[size];
      for(int i=0;i<size;i++){
        a[i]=sc.nextInt();
      }

      long starTime=System.nanoTime();
      int c=bs.Search(a,size);
      long estimatedTime=System.nanoTime()-starTime;

      System.out.println(size+" "+estimatedTime);
    }
  }
}
