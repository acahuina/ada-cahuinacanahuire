import java.util.*;

public class Palindrome{
  public static void main(String[]args){
    if(args.length<1){
      System.err.println("Usage: $java Palindrome <word review>");
      System.exit(1);
    }
    String word=(String)(args[0]);
  
    System.out.println(CheckPalindrome(word));
  }

  private static boolean CheckPalindrome(String word){
    int left=0;
    int right=word.length()-1;
    while(left<right && word.charAt(left)==word.charAt(right)){
      left=left+1;
      right=right-1;
    }
    if(left>=right)
      return true;
    else
      return false;
  }
}
