import java.util.*;

public class SortingMergeSort{
  public static void main(String[]args){
    Scanner sc=new Scanner(System.in);
    MSort merge=new MSort();

    int times=100;
    double[]data=new double[times];

    while(sc.hasNext()){
      int size=sc.nextInt();
      int[]a=new int[size];

      for(int i=0;i<size;i++){
        a[i]=sc.nextInt();
      }

      Statistics s=null;
      int maxTry=1000;
      double min=Double.MAX_VALUE;
      double bestEstimated=0;
      do{
        int[]b=new int[size];
        System.arraycopy(a,0,b,0,a.length);
        long estimatedTime=0;
        for(int i=0;i<times;i++){
          long startTime=System.nanoTime();
          merge.mergeSort(b,1,b.length);
          estimatedTime=System.nanoTime()-startTime;
          data[i]=estimatedTime;
        }
        s=new Statistics(data);
        maxTry--;
        if(s.getVariance()<min){
          min=s.getVariance();
          bestEstimated=estimatedTime;
        }
      }while(maxTry>0 && s.getVariance()>=5);
      System.out.println(size+" "+s.getMean());
      System.err.println(s.getMean()+" "+s.getVariance()+" "+s.getStdDev()+" "+s.median());
    }
  }
}
