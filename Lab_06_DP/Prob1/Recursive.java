import java.util.*;

class Recursive{
  public static int[][]precios={{4,6,8},{5,10},{1,3,5,5}};
  public static void main(String[]args){
    //int[][]precios={{4,6,8},{5,10},{1,3,5,5}};
    int c=3;
    int m=20;
    int i=0;
    int resp=DP(m,c,i);
    System.out.println(resp);
    System.out.println(m-resp);
    //System.out.println(precios.length);
    //System.out.println(precios[0].length);
    //System.out.println(precios.length);
    //System.out.println(precios[1].length);
    //System.out.println(precios.length);
    //System.out.println(precios[2].length);
  }
  public static int DP(int m,int c,int indice){
    
    if(m<0)
      m=Integer.MAX_VALUE;
    if(c==0)
      return m;
    else{
      int min=Integer.MAX_VALUE;
      c=c-1;
      indice=indice+1;
      for(int i=0;i<precios[indice-1].length;i++){
        min=Math.min(min,DP(m-precios[indice-1][i],c,indice));
      }
      return min;
    }
  }
}
