import java.util.*;
import java.io.*;

public class MatrixGenerator{
  private static int[][]generateMatrix(int number){
  
      int[][]matrix=new int[number][number];
      for(int i=0;i<matrix.length;i++)
        for(int j=0;j<matrix[i].length;j++){
          matrix[i][j]=(int)(Math.random()*100);
        }
    return matrix;
  }

  public static void print(PrintStream out, int [][]a){
   
      for(int j=0;j<a.length;j++){
        out.println();
        for(int k=0;k<a[j].length;k++){
          out.print(a[j][k]+" ");
        }
      }
      out.println();
  }
  public static void main(String[]args){
    if(args.length<1){
      System.err.println("Usage: $java MatrixGenerator <number of arrays>");
      System.exit(1);
    }
    int number=Integer.parseInt(args[0]);
    
    int size=2;
    for(int i=1;i<=number;i++){
      print(System.out,generateMatrix(size));
      if(i%2==0){
        size=size+1;
      } 
    }
  }

}
